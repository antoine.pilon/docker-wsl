# Pulling from Amazon public registry to avoid pull limit of DockerHUB
FROM public.ecr.aws/debian/debian:11-slim

# Skip interactive install of docker
ENV DEBIAN_FRONTEND=noninteractive

# Clean previous docker install
# Install dependencies
# Install Python3 & pip for docker-compose
# Add docker's key to package manager
# Install docker
# Install compose
# Setup docker_host env var for root user
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get remove --autoremove -y \
        docker \
        docker.io \
        containerd \
        runc \
    && apt-get install -y \
        --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release \
        python3 \
        python3-pip \
    &&  curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
    && echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
        $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update \
    && apt-get install -y \
        --no-install-recommends \
        docker-ce \
        docker-ce-cli \
        containerd.io \
    && update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy \
    && update-alternatives --set iptables /usr/sbin/iptables-legacy \
    && python3 -m pip install docker-compose \
    && echo "export DOCKER_HOST=tcp://localhost:2375/" >> /root/.bashrc \
    && echo "dockerd" >> /root/.bashrc

ADD daemon.json /etc/docker/daemon.json

RUN ln -s /mnt/c /c

ADD setup-proxy.py /