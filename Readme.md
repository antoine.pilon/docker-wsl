# Docker wsl

This repository provides starter pack to run Docker on Windows 10 using WSL2 engine.

## TL;DR

1. Download latest docker-wsl archive by following [this link](https://gitlab.com/antoine.pilon/docker-wsl/-/jobs/2027175165/artifacts/file/docker-wsl-0.5.tar)

2. Execute following commands in admin powershell
```powershell
$ErrorActionPreference = "Stop"
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V
Enable-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform
bcdedit /set hypervisorlaunchtype auto
```

3. Run in NON-admin powershell
```powershell
wsl --update
wsl --set-default-version 2
[Environment]::SetEnvironmentVariable("DOCKER_HOST","tcp://localhost:2375","User")
wsl --import docker-wsl C:\docker-wsl docker-wsl-0.5.tar # Edit as you wish 'wsl --import <distro-name> <distro-storage-path> <archive-path>
wsl --list --verbose # Distro should be visible and version should be 2
```

2. Launch Docker daemon
```powershell
wsl -d docker-wsl
```

3. Download [Docker-CLI](https://download.docker.com/win/static/stable/x86_64/) and extract docker.exe in path, then execute docker commands in separate powershell window
```docker
docker run hello-world # Output seems bugged on windows, but you shouldn't have any errors
```

4. *Optional* Install docker-compose :
```powershell
python -m pip install docker-compose
[Environment]::SetEnvironmentVariable("COMPOSE_CONVERT_WINDOWS_PATHS","1","User")
```

## Requirements

<details>
<summary>WSL 2</summary>
<br>

According to [microsoft website, "You must be running Windows 10 version 2004 and higher (Build 19041 and higher) or Windows 11"](https://docs.microsoft.com/fr-fr/windows/wsl/install).

If running Windows 10 build lower than 19041 or if the command `wsl --install` does not works, try as follows.

- If Virtualbox is installed, ensure the version is at least **6.1**
- Ensure **virtualisation is fully enabled in BIOS**
- Enable / Disable following Windows optional features :
  * **ENABLE** -> Windows Subsystem for Linux
  ```powershell
  Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
  ```
  * **ENABLE** -> Virtual Machine Platform
  ```powershell
  Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform
  ```
  * **DISABLE** -> Hyper-V
  ```powershell
  Disable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V
  ```
  * If you want to use virtualbox alongside WSL 2, then **ENABLE** -> Windows Hypervisor Platform
  ```powershell
  Enable-WindowsOptionalFeature -Online -FeatureName Windows Hypervisor Platform
  ```
- Ensure hypervisor type is to **auto**
```powershell
bcdedit /set hypervisorlaunchtype auto
```
- You might have to upgrade WSL 2 Linux kernel, [download and install wsl-update](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi) and/or try command below
```powershell
wsl --update
```
</details>

<details>
<summary>Docker-CLI</summary>
<br>

The docker-cli communicates with the Docker daemon. If you want to execute docker command directly from windows powershell, cmd, git bash etc... (i.e. outside of the WSL console), then :
- Download Docker-CLI and extract docker.exe somewhere in your `$PATH`, latest Docker-CLI releases can be found at https://download.docker.com/win/static/stable/x86_64/. (Should stay free, [sources available here](https://github.com/docker/cli))
- Set environment variable `DOCKER_HOST=tcp://localhost:2375/`, so that the docker cli will interact with the WSL Daemon.

</details>

<details>
<summary>Docker-compose</summary>
<br>

Docker-compose is available on both Windows and Linux using a pip repository.

- Install Python (check the box 'install pip' and 'add python to PATH' during install)
- Run the following command to install docker-compose
```powershell
python -m pip install docker-compose
```
- Set the following environnement variable `COMPOSE_CONVERT_WINDOWS_PATHS=1`. It will convert awful Windows pathes (C:\\....) in unix pathes (/c/) when working with compose.

Docker-compose executable should be available in your path. If not, then check pip logs, it should warn you if the executable is not in yout path. The nadd the given path to your `$PATH`.
</details>

## How to use behind (corporate) proxy ?

### TL;DR

```powershell
wsl -d docker-wsl python3 /setup-proxy.py --http-proxy=<http_proxy_url> --https-proxy=<https_proxy_url>
```

Or, if using windows host as a proxy (i.e. windows proxy configured as localhost or 127.0.0.1)
```powershell
wsl -d docker-wsl python3 /setup-proxy.py -l
```

<details>
<summary>Proxy config in linux</summary>
<br>

Most of linux applications use environment variables `HTTP_PROXY`, `HTTPS_PROXY`, `NO_PROXY` as proxy configurations. Then, if using standard proxy, you should use these varaibles. Nethertheless, Ubuntu/Debian package manager `apt-get` uses its own proxy configuration. Then, you should also configure proxy in `/etc/apt/apt.conf.d/proxy.conf` as follows :
```
Acquire::http::Proxy "http://user:password@proxy.server:port/";
Acquire::https::Proxy "http://user:password@proxy.server:port/";
```

Moreover, setting system-wide environment variables in linux is usually achieved by adding these vars in `/etc/environment` file, which is read by systemd at startup. Unfortunately, systemd is not provided in wsl (doesn't work by default since it does not have PID 1), and is a pain in the a** to configure (if interested see [systemd genie](https://github.com/arkane-systems/genie)), so all you have to do is add env variables configuration in `~/.bashrc` (root by default) so that everytime you open a window, then the env vars are set :
```bash
...
export HTTP_PROXY=<http_proxy>
export HTTPS_PROXY=<https_proxy>
export NO_PROXY=<no_proxy>
```

If you add new users in your docker-wsl distro, then you will have to cinfigure user's `~/.bashrc`.

> **NOTE if proxying to Windows Host**: If you use a localhost proxy on your windows machine, then you shouldn't use 127.0.0.1 or localhost in WSL. It is because WSL distro will have its own virtual network interfaces and IP adresses. You then have to set up your proxy with the IP address of your Windows. To find the IP set to your windows host, run `cat /etc/resolv.conf`, the default nameserver is the windows host. Sadly, the IP isn't static and will likely change at each reboot / update etc... A simple trick to always get the right IP is to get it from this file automatically, by adding these lines in your `.bashrc` configuration :
```bash
export HTTP_PROXY=<http_proxy>
export HTTPS_PROXY=<https_proxy>
```

</details>

## Customize my own distribution

If the default distro provided in .tar file does not fit your requirements, then you can easimy build your own using any container build/run tool. For instance, using podman :

```shell
# Edit the Containerfile, for instance change the base image from Ubuntu to Debian
podman build -t <my_image_name> .
podman run <my_image_name>
podman ps -a # Find the id of the container you just ran
podman export -o <my_archive_name>.tar <my_container_id>
```

Then, use the generated archive instead of default one in powershell

## Tips

### Several docker-wsl distro

TODO

### Minimize powershell in taskbar

TODO

### Use non root user in wsl

TODO
